1. Fa�a um algoritmo que receba 4 (quatro) n�meros, calcule e mostre a soma dos n�meros;

2. Fa�a um algoritmo que receba 3 (tr�s) notas, calcule e mostre a m�dia aritm�tica entre elas;

3. Fa�a um algoritmo que receba o nome e o sal�rio de um funcion�rio, calcule e mostre o nome do funcion�rio e seu novo sal�rio, sabendo-se que este sofreu um aumento de 15%.

4. Fa�a um algoritmo que receba o nome, o sal�rio de um funcion�rio e o percentual de aumento, calcule e mostre o novo sal�rio.

5. Fa�a um algoritmo que receba o sal�rio-base de um funcion�rio, calcule e mostre o sal�rio a receber, sabendo que esse funcion�rio tem uma gratifica��o de 5% sobre o sal�rio-base e paga imposto de 7% sobre o sal�rio-base.

6. Fa�a um algoritmo que calcule e mostre a �rea de um triangulo. �rea do triangulo = (Base x Altura)/2

7. Fa�a um algoritmo que receba o Ano de Nascimento de uma pessoa e o ano atual, calcule e mostre: A idade Atual dessa pessoa e quantos anos essa pessoa tinha em 2016.

8. Pedro Comprou um saco de Ra��o com peso em quilos. Pedro possui 2 (dois) gatos para os quais fornece a quantidade de ra��o em gramas diferentes para cada gato. Fa�a um algoritmo que receba o peso do Saco de ra��o e a quantidade de ra��o fornecida para cada gato. Calcule e mostre quanto restar� de ra��o no saco ap�s 5 (cinco) dias.